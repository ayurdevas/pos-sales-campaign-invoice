# -*- coding: utf-8 -*-
{
    'name': "POS Sales Campaign to Invoice",
    'summary': "Set Sales Campaign when invoicing from POS Order",
    'author': "Roberto Sierra <roberto@ideadigital.com.ar>",
    'website': "https://github.com/ayurdevas/pos-sales-campaign-invoice",
    'license': 'LGPL-3',
    'category': 'Sales/Point Of Sale',
    'version': '12.0.0.1.0',
    'depends': [
        'pos_sale_invoice',
        'sales_campaign',
    ],
}
