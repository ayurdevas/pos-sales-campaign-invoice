# -*- coding: utf-8 -*-

from odoo import models


class PosOrder(models.Model):
    _inherit = 'pos.order'

    def _prepare_invoice(self):
        vals = super(PosOrder, self)._prepare_invoice()

        if 'team_id' in vals:
            team_id = self.env['crm.team'].browse(vals['team_id'])
            campaign = self.env['sales.campaign'].search_campaign(self.date_order, team_id)
            vals['campaign_id'] = campaign.id if campaign else False

        return vals
